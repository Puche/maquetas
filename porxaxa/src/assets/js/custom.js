/* jshint node: true */
/* jshint esversion: 6 */

import $ from 'jquery';
import slick from 'slick-carousel';
import magnificPopup from 'magnific-popup';

$( document ).ready(function() {

    $('.js-toggle-class').on('click', function(e) {
        e.preventDefault();
        $($(this).data('target')).toggleClass($(this).data('class'));
    });

    $('.js-main-slider').slick({
        arrows: true,
        prevArrow: '<button class="c-main-slider__arrow c-main-slider__arrow--prev c-icon-arrow-left"></button>',
        nextArrow: '<button class="c-main-slider__arrow c-main-slider__arrow--next c-icon-arrow-right"></button>',
        dots: true
    });

    $('.js-slider-posts').slick({
        arrows: true,
        prevArrow: '<button class="c-posts__arrow c-posts__arrow--prev c-icon-arrow-left"></button>',
        nextArrow: '<button class="c-posts__arrow c-posts__arrow--next c-icon-arrow-right"></button>',
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    $('.js-slider-brands').slick({
        arrows: false,
        mobileFirst: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6
                }
            }
        ]
    });

    $('.js-filter').on('click', function(e) {
        e.preventDefault();
        let filter = $(this).data('filter');

        $(this).parent().find('a').removeClass('is-active');
        $(this).toggleClass('is-active');

        if (filter == 'all') {
            $('.js-filter-target').find('> *')
            .removeClass('u-bounceIn')
            .addClass('is-active')
            .addClass('u-bounceIn');
        } else {
            $('.js-filter-target').find('> *')
            .not('.' + filter)
            .removeClass('u-bounceIn')
            .removeClass('is-active');


            $('.js-filter-target').find('.' + filter)
            .removeClass('u-bounceIn')
            .addClass('is-active')
            .addClass('u-bounceIn');
        }
    });

    if ($('.js-house').length > 0) {
        $('.js-house').magnificPopup();
    }

    $('.js-thumbs-post').on('click', function(e){
        e.preventDefault();
        var src = $(this).find('img').attr('src');
        var srcBig = $(this).parents('.c-service-details').find('.c-service-details__img').attr('src', src);
    });

    $('.js-map').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('is-active');
        $(this).toggleClass('is-active');
    });

});