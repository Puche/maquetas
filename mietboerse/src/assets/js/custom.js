$( document ).ready(function() {
    $('.js-main-nav').on('click', function(e){
        e.preventDefault();
        $(this).parents('.c-main-header').find('.c-main-header__nav').toggleClass('is-active');
        $(this).toggleClass('is-active');
    });

    $('.js-card-view').on('click', function(e){
        e.preventDefault();
        $(this).parents('.o-layout').find('.o-layout__row').toggleClass('o-layout__row--alt-view');
        $(this).parents('.o-layout').find('.c-card').toggleClass('c-card--theme-2');
    });

    $('.js-parallax').parallax();

    $('.js-gallery').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
        delegate: 'a'
    });

    if ($(".sticky").length) {
        var sticky = new Waypoint.Sticky({
            element: $('.sticky')[0],
            handler: function() {},
            wrapper: false
        });

        $('.c-footer').waypoint(function(direction) {
            $('.sticky').toggleClass('stuck', direction === 'up');
            $('.sticky').toggleClass('sticky-surpassed', direction === 'down');
        }, {
            offset: function() {
                return $('.sticky').outerHeight();
            }
        });
    }
});
