$( document ).ready(function() {
    $('.js-main-nav').on('click', function(e){
        e.preventDefault();
        $('.js-main-nav-target').toggleClass('is-active');
        $(this).toggleClass('is-active');
    });

    $('.js-lang').on('click', function(e){
        e.preventDefault();
        $('.js-lang-target').toggleClass('is-active');
        $(this).toggleClass('is-active');
    });

    $('.js-map').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('is-active');
        $(this).toggleClass('is-active');
    });

    $('.js-gallery').magnificPopup({
        delegate: '.c-gallery__item',
        type: 'image',
        gallery:{
            enabled: true
        }
    });

    $('.js-gallery-iframe').magnificPopup({
        delegate: '.c-gallery__item',
        type: 'iframe',
        gallery:{
            enabled: true
        }
    });

    $('.js-video').magnificPopup({
        type: 'iframe',
    });
});
